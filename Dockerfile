FROM node

WORKDIR /usr/src/app

COPY . .

RUN npm install

EXPOSE 3000

ENV NEXTAUTH_URL="https://cesi.alexandre-hublau.com"

CMD [ "npm", "run", "start" ]
