import Head from "next/head";
import styles from "../styles/Home.module.css";
import Link from "next/link";
import { signIn, signOut, useSession } from "next-auth/client";

export default function Layout({ children }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>CESI</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <h1 className={styles.title}>
          <Link href="/">
            <a>CESI DEMO</a>
          </Link>
        </h1>

        {children}
      </main>

      <footer className={styles.footer}>
        <a>Alexandre HUBLAU</a>
      </footer>
    </div>
  );
}
