import Head from "next/head";
import styles from "../styles/Home.module.css";
import Layout from "../components/Layout";

export default function About() {
  return (
    <div className={styles.container}>
      <Head>
        <title>A propos</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <p className={styles.description}>Page à propos</p>
      </Layout>

      {/* <main className={styles.main}>
        <h1 className={styles.title}>A propos</h1>

        <p className={styles.description}>A propos</p>
      </main>

      <footer className={styles.footer}>
        <a>CESI </a>
      </footer> */}
    </div>
  );
}
