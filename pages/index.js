import Layout from "../components/Layout";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import Link from "next/link";
import { signIn, signOut, useSession } from "next-auth/client";

export default function Home() {
  const [session, loading] = useSession();

  return (
    <Layout>
      <p className={styles.description}>Page faite avec ReactJS</p>

      <div className={styles.grid}>
        <Link href="/a-propos">
          <a className={styles.card}>
            <h3>Page à propos &rarr;</h3>
            <p>A propos</p>
          </a>
        </Link>

        {loading ? (
          <a className={styles.card}>
            <h3>Chargement &rarr;</h3>
          </a>
        ) : (
          <>
            {!session ? (
              <div className={styles.card} onClick={signIn}>
                <h3>Connexion &rarr;</h3>
                <p>Connexion</p>
              </div>
            ) : (
              <div className={styles.card} onClick={signOut}>
                <h3>{session.user.email} &rarr;</h3>
                <p>Se déconnecter</p>
              </div>
            )}
          </>
        )}

        <Link href="/profile">
          <a className={styles.card}>
            <h3>Mon profil &rarr;</h3>
            <p>Voir son profil</p>
          </a>
        </Link>

        <a className={styles.card}>
          <h3>-</h3>
          <p>-</p>
        </a>
      </div>
    </Layout>
  );
}
