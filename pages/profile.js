import { useState, useEffect } from "react";
import { useSession } from "next-auth/client";
import Layout from "../components/Layout";
import styles from "../styles/Home.module.css";

export default function Page() {
  const [session, loading] = useSession();

  // When rendering client side don't display anything until loading is complete
  if (typeof window !== "undefined" && loading) return null;

  // If no session exists, display access denied message
  if (!session) {
    return (
      <Layout>
        <p className={styles.description}>
          Erreur : vous n'êtes pas connecté !
        </p>
      </Layout>
    );
  }

  // If session exists, display content
  return (
    <Layout>
      <p className={styles.description}>
        Bonjour{" "}
        <strong>
          {session.user.name} | {session.user.email}
        </strong>{" "}
        !
      </p>
    </Layout>
  );
}
